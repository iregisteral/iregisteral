iRegister is .AL Domain Accredited registrar that operates its own data center in Tirana, Albania. Our reliable network, professional services, and excellent customer care have made iRegister a trusted partner for market leaders in brand protection, network, privacy-related service providers.

Address: Rr. Abdi Toptani, Torre Drini, Kati II, Zyra 27/28, Tirana,  1001, Albania
Phone: +355 68 902 8040
Website: https://iregister.al